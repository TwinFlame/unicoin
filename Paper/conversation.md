On Wörgl, Austria - I have a prototype for something called "Paper Protocol" that could replace Fiat, with a system of recycleable banknotes based on QR codes?
It's really not that hard to build an alternative Financial system.

It's based on Locks & Keys. When a coin or banknote is Spent, it gets locked. It has to get "refilled" and Unlocked by it's new owner. It's key is tied to something Authenticatable, like a Merchant StoreID#, or your personal Crypto bank account.  Hence you can have an ATM that exists in Software Only! Print your own currency. Once you transfer it to someone else, It is Locked again, and has no value, until loaded with Bitcoin/Gold or something of Value.

*Has to works totally without the Holder having a digital wallet.* For countries that are not as developd.. Perhaps you could go to a Kiosk and deposit or reload Banknotes values? Like how BTC.com and others have a "Cloud Wallet" App?

https://github.com/UniBank/Paper-Banknote

Banknotes "top up" in stable denominations, of like $10 $20  or like 1 gram Gold, 10 gram Gold, platnum. etc.  [ Gold or Commodity Backed Assets ]
.

I think most of the design spec I'm started is pretty Inutitive? It's definitely Not revolutionary. And no stranger to anyone who knows basic PK Cryptography, and has used reloadable debit cards before? If you've ever gotten a single use Rebate Prepaid Card, for buying things at reailers like Newegg? Same idea. But instead of having one card you swipe and "Keep" you transfer it to someone else. 
Then it locks, and they have to Authenticate with their ID & unlock with their it to Prove they didn't steal your money. 

Deregistering Stolen Notes: 
IF you were to report your money was stolen: That QR Code is deregistered from the System, and the Thief cannot ever Spend or unlock that note ever again.  Then you just print a New note with a QR code and load it, and you never lost any money at all...  This way IT also works like integer denominated Checkques!

banks are already masters at defendingagainst this: https://www.lifelock.com/learn-fraud-what-is-check-fraud.html  I Need to literally Recruit someone who's worked in one. In the ACH field. https://www.investopedia.com/terms/a/ach.asp  Also I think Ethereum Classic has "reversible transactions"? I need to  talk to them about whether or not they could simplify it even further

If I Coded it? I'd like someone we trust take over the Project. and be the Leader of all further Community efforts around replacing real-world money. Could be you?
It's open source, I Want people to Fork it! As I'm so overcommitted & backlogged as it is, I may never be able to..
